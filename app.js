const express = require('express');
const sqlite3 = require('sqlite3');
const app = express();
const port = 3000;
const db = new sqlite3.Database('mydatabase.db');

// Initialize the database table if it doesn't exist
db.serialize(() => {
    db.run('CREATE TABLE IF NOT EXISTS products (id INTEGER PRIMARY KEY, name TEXT, description TEXT, price REAL, tags TEXT)');
});

app.use(express.json());

// Create - Add a new product
app.post('/products', (req, res) => {
    const { name, description, price, tags } = req.body;
    db.run('INSERT INTO products (name, description, price, tags) VALUES (?, ?, ?, ?)', [name, description, price, tags], function (err) {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.json({ id: this.lastID });
    });
});

// Read - Get all products
app.get('/products', (req, res) => {
    db.all('SELECT * FROM products', (err, rows) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.json({ products: rows });
    });
});

// Read - Get a single product by ID
app.get('/products/:id', (req, res) => {
    const { id } = req.params;
    db.get('SELECT * FROM products WHERE id = ?', [id], (err, row) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.json({ product: row });
    });
});

// Update - Update a product by ID
app.put('/products/:id', (req, res) => {
    const { id } = req.params;
    const { name, description, price, tags } = req.body;
    db.run('UPDATE products SET name = ?, description = ?, price = ?, tags = ? WHERE id = ?', [name, description, price, tags, id], function (err) {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.json({ message: 'Product updated successfully' });
    });
});

// Delete - Delete a product by ID
app.delete('/products/:id', (req, res) => {
    const { id } = req.params;
    db.run('DELETE FROM products WHERE id = ?', [id], function (err) {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.json({ message: 'Product deleted successfully' });
    });
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
